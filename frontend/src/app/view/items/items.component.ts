import { HeaderService } from './../../components/template/header/header.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  constructor(private headerService: HeaderService,
    private router: Router) {
    headerService.headerModel = {
      title: 'Cadastro de itens',
      icons: 'storefront',
      routerUrl: '/items'
    }
  }

  ngOnInit(): void {
  }
  navigateItemCreate() {
    this.router.navigate(["/items/create"]);
  }
}
