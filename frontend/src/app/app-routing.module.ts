import { ItemDeleteComponent } from './components/item/item-delete/item-delete.component';
import { ItemCreateComponent } from './components/item/item-create/item-create.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './view/home/home.component';
import { ItemsComponent } from './view/items/items.component';
import { ItemUpdateComponent } from './components/item/item-update/item-update.component';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  }, {
    path: "items",
    component: ItemsComponent
  },
  {
    path: "items/create",
    component: ItemCreateComponent
  },
  {
    path: "items/update/:id",
    component: ItemUpdateComponent
  },
  {
    path: "items/delete/:id",
    component: ItemDeleteComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
