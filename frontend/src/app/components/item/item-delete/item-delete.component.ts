import { Item } from './../item.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-item-delete',
  templateUrl: './item-delete.component.html',
  styleUrls: ['./item-delete.component.css']
})
export class ItemDeleteComponent implements OnInit {
  item: Item
  constructor(private router: Router,
    private itemService: ItemService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.itemService.readById(id).subscribe(item => this.item = item)
  }

  deleteItem(): void {
    this.itemService.delete(this.item.id.toString()).subscribe(() => {
      this.itemService.show("Item excluido com sucesso")
      this.returnPage()
    })
  }

  returnPage(): void {
    this.router.navigate(['/items'])
  }

}
