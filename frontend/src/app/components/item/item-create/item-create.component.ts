import { Item } from './../item.model';
import { Router } from '@angular/router';
import { ItemService } from './../item.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.css']
})
export class ItemCreateComponent implements OnInit {
  item: Item = {
    name: '',
    price: null
  }
  constructor(private itemService: ItemService, private router: Router) { }

  ngOnInit(): void {

  }
  createOk(): void {
    this.itemService.createItem(this.item).subscribe(
      () => {
        this.itemService.show("Item criado !!!");
        this.returnPage()
      }
    )

  }
  returnPage(): void {
    this.router.navigate(['/items'])
  }


}
