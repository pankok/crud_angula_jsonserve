import { Item } from './../item.model';
import { ItemService } from './../item.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-update',
  templateUrl: './item-update.component.html',
  styleUrls: ['./item-update.component.css']
})
export class ItemUpdateComponent implements OnInit {
  item: Item
  constructor(private router: Router,
    private itemService: ItemService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.itemService.readById(id).subscribe(item => this.item = item)
  }
  updateItem(): void {
    this.itemService.update(this.item).subscribe(() => {
      this.itemService.show("Item alterado com sucesso")
      this.returnPage()
    })
  }

  returnPage(): void {
    this.router.navigate(['/items'])
  }

}
