import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';
import { Item } from '../item.model';

@Component({
  selector: 'app-item-read',
  templateUrl: './item-read.component.html',
  styleUrls: ['./item-read.component.css']
})
export class ItemReadComponent implements OnInit {
  itemList: Item[]
  displayedColumns = ['id', 'name', 'price', 'action']
  constructor(private itemService: ItemService) { }

  ngOnInit(): void {
    this.itemService.readItem().subscribe(items => this.itemList = items)
  }

}
