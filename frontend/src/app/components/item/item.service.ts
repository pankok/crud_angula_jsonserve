import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Item } from './item.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ItemService {
  urlBase: string = "http://localhost:3001/items"
  constructor(private snackbar: MatSnackBar, private http: HttpClient) { }


  show(msg: string) {
    this.snackbar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top",
    })
  }

  createItem(item: Item): Observable<Item> {
    return this.http.post<Item>(this.urlBase, item)
  }

  readItem(): Observable<Item[]> {
    return this.http.get<Item[]>(this.urlBase)
  }
  readById(id: string): Observable<Item> {
    const url = `${this.urlBase}/${id}`
    return this.http.get<Item>(url)
  }

  update(item: Item): Observable<Item> {
    const url = `${this.urlBase}/${item.id}`

    return this.http.put<Item>(url, item)
  }

  delete(id: string): Observable<Item> {
    const url = `${this.urlBase}/${id}`
    return this.http.delete<Item>(url)
  }

}
