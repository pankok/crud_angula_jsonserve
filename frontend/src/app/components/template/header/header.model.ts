import { ItemsComponent } from './../../../view/items/items.component';
export interface HeaderModel {
    title: string,
    icons: string,
    routerUrl: string
}